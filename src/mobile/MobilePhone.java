package mobile;

import java.util.ArrayList;

public class MobilePhone {
	private String number;
	private ArrayList<Contact> contact;

	public MobilePhone(String number) {
		super();
		this.number = number;
		this.contact = new ArrayList<Contact>();
	}

	private int findContact(Contact contact) {
		int position = this.contact.indexOf(contact);
		if (position < 0) {
			System.out.println("Contact not found");
		}
		return position;
	}

	private int findContact(String name) {
		for (int i = 0; i < this.contact.size(); i++) {
			if (name.equals(this.contact.get(i).getName())) {
				System.out.println("Name " + name + " is found in Contact");
				return i;

			}
		}
		System.out.println("Name " + name + " is  not found in Contact");
		return -1;
	}

	public void printContact() {
		for (int i = 0; i < this.contact.size(); i++) {
			System.out.println(+i + 1 + "name " + this.contact.get(i).getName() + " phonenumber "
					+ this.contact.get(i).getNumber());
		}if(this.contact.size()==0) {
			System.out.println("you dont have any conatcts saved");
		}
	}

	public boolean addNewContact(Contact contact) {
		int positionC = findContact(contact);

		int positionN = findContact(contact.getName());
		if (positionC >= 0 && positionN >= 0) {
			return false;
		}
		this.contact.add(contact);
		System.out.println("New contact " + contact.getName() + " added");
		return true;
	}

	public boolean updateContact(Contact oldContact, Contact newContact) {
		int positionC = findContact(oldContact);
		if (positionC < 0) {
			System.out.println("Contact " + oldContact.getName() + " not found");
			return false;
		}
		int positionN = findContact(oldContact.getName());
		if (positionC < 0 || positionN < 0) {
			System.out.println("Contact " + oldContact.getName() + " not found");
			return false;

		}
		this.contact.set(positionC, newContact);
		System.out.println("old contact " + oldContact.getName() + " is replaced with " + newContact.getName());
		return true;

	}

	public boolean removeContact(Contact contact) {
		int postionC = this.findContact(contact);
		if (postionC < 0) {
			System.out.println("Contact " + contact.getName() + " not found");
			return false;
		}
		this.contact.remove(postionC);
		System.out.println("Contact " + contact.getName() + " removed");
		return true;

	}

	public String queryContact(Contact contact) {
		int positionC = findContact(contact);
		if (positionC < 0) {
			System.out.println("Contact " + contact.getName() + " not found");
			return null;
		}
		return contact.getName();

	}

	public Contact queryContact(String name) {
		int positionN = findContact(name);
		if (positionN < 0) {
			System.out.println("Contact with name " + name + " not found");
			return null;

		}
		return this.contact.get(positionN);
	}

}
