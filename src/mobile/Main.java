package mobile;

import java.util.Scanner;



public class Main {
	public static Scanner scanner = new Scanner(System.in);
	public static MobilePhone mobilePhone = new MobilePhone("8770079880");

	public static void main(String[] args) {
		startPhone();
		printActions();
		boolean quit = false;
		while (!quit) {
			System.out.println("Enter Action :");
			int choice = scanner.nextInt();
			scanner.nextLine();
			switch (choice) {
			case 0:
				System.out.println("\nShutting down...");
				quit = true;
				break;
			case 1:
				mobilePhone.printContact();
				break;
			case 2:
				addNewContact();
				break;

			case 3:
				updateContact();
				break;

			case 4:
				removeContact();
				break;

			case 5:
				queryContact();
				break;

			case 6:
				printActions();
				break;
			}

		}

	}

	private static void queryContact() {
		
		System.out.println("Enter the conatct name :");
		String oldName = scanner.nextLine();
		Contact existingContactRecord = mobilePhone.queryContact(oldName);
		if(existingContactRecord == null) {
            System.out.println("Contact not found.");
            return;
        }
	}

	private static void removeContact() {
		System.out.println("Enter the conatct name :");
		String oldName = scanner.nextLine();
		Contact existingContactRecord = mobilePhone.queryContact(oldName);
		if(existingContactRecord == null) {
            System.out.println("Contact not found.");
            return;
        }
		mobilePhone.removeContact(existingContactRecord);

	}

	private static void updateContact() {
		System.out.print("Enter old contact name: ");
		String oldName = scanner.nextLine();
		Contact existingContactRecord = mobilePhone.queryContact(oldName);
		if(existingContactRecord == null) {
            System.out.println("Contact not found.");
            return;
        }
		System.out.print("Enter new contact name: ");
		String newName = scanner.nextLine();
		System.out.print("Enter new contact phone number: ");
		String newNumber = scanner.nextLine();
		Contact newContact = Contact.createContact(newName, newNumber);
		mobilePhone.updateContact(existingContactRecord, newContact);

	}

	private static void addNewContact() {
		System.out.println("Enter the name : ");
		String name = scanner.nextLine();
		System.out.println("Enter the number : ");
		String number = scanner.nextLine();
		Contact contact = Contact.createContact(name, number);
		mobilePhone.addNewContact(contact);

	}

	private static void startPhone() {
		System.out.println("Starting phone...");
	}

	private static void printActions() {
		System.out.println("\nAvailable actions:\npress");
		System.out.println("0  - to shutdown\n" + "1  - to print contacts\n" + "2  - to add a new contact\n"
				+ "3  - to update an existing contact\n" + "4  - to remove an existing contact\n"
				+ "5  - query if an existing contact exists\n" + "6  - to print a list of available actions.");
		System.out.println("Choose your action: ");
	}

}
