package mobile;

public class Contact {
	private String name;
	private String phoneNumber;
	
	
	public Contact(String name, String number) {
		super();
		this.name = name;
		this.phoneNumber = number;
	}
	public String getName() {
		return name;
	}
	
	public String getNumber() {
		return phoneNumber;
	}
	public static Contact createContact(String name,String number) {
		return new Contact(name, number);
		
	}
	
	
	
	
}
